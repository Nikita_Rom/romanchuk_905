package ru.kpfu.itis.group905.romanchuk.game;

import java.util.Scanner;

public class Game {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int q = 0;

		System.out.println("What is your name, player 1?");
		String name1 = sc.nextLine();
		System.out.println("What is your name, player 2?");
		String name2 = sc.nextLine();

		Player p1 = new Player(name1);
		Player p2 = new Player(name2);

		while (q != 1) {
			System.out.println(p1.getName(name1) + " hits:");
			int power = sc.nextInt();
			if ((power > 0) & (power < 10)) {
				p2.damage(power);
			}
			else {
				System.out.println("Error");
			}

			if (p2.getHp() <= 0) {
				System.out.println("Game over, win " + p1.getName(name1));  
				q = 1;
  				break;
			}

			System.out.println(p2.getName(name2) + " hits:");
			power = sc.nextInt();
			if ((power > 0) & (power < 10)) {
				p1.damage(power);
			}
			else {
				System.out.println("Error");
			}


			if (p2.getHp() <= 0) {
				System.out.println("Game over, win " + p2.getName(name2)); 
				q = 1;      
			}
		}
	}
}