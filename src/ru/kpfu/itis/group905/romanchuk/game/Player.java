package ru.kpfu.itis.group905.romanchuk.game;

public class Player {
	private String name;
	private int hp;


	Player(String name) {
		hp = 100;
		this.name = name;
	}

	public int getHp() {
		return this.hp;
	}

	public String getName(String name) {
		return this.name;
	}

	public void damage(int power) {
		hp -= power;
	}
}