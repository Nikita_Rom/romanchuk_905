package ru.kpfu.itis.group905.romanchuk.complexClasses;

import ru.kpfu.itis.group905.romanchuk.basicClasses.RationalFraction;

public class RationalVector2D {

	private RationalFraction x;
	private RationalFraction y;


	public RationalFraction getX() {
		return this.x;
	}
	public RationalFraction getY() {
		return this.y;
	}

	public RationalVector2D(RationalFraction v1, RationalFraction v2) {
		this.x = v1;
		this.y = v2;
	}  

	public RationalVector2D() {
		this(new RationalFraction(), new RationalFraction());
	}

	public String toString() {
    	return (this.x.toString() + ";" + this.y.toString());
    }

    public RationalVector2D add(RationalVector2D secondVector) {
    	RationalVector2D result = new RationalVector2D(
    		this.x.add(secondVector.x),
    		this.y.add(secondVector.y)
    	);
    	return result;
    }

    public double length() {
     	return Math.sqrt(this.x.mult(this.x).add(this.y.mult(this.y)).value(this.x.getNumerator(),this.y.getDenominator()));
    }	

    public double scalarProduct(RationalVector2D secondVector) {
    	return  (this.x.mult(secondVector.x).value(this.x.getNumerator(),this.y.getDenominator()) + this.y.mult(secondVector.y).value(this.x.getNumerator(),this.y.getDenominator()));
    }

    public boolean equals(RationalVector2D secondVector) {
    	if ((this.x.equals(secondVector.x) == true) & (this.y.equals(secondVector.y) == true)) {
    		return true;
    	}
    	else { return false; }	
    } 
}
