package ru.kpfu.itis.group905.romanchuk.complexClasses;

import ru.kpfu.itis.group905.romanchuk.basicClasses.RationalFraction;

import java.util.Scanner;

public class Main {
	 
	 public static void main(String[] args) {
	 	RationalFraction v1 = new RationalFraction(1, 3);
	 	RationalFraction v2 = new RationalFraction(1, 2);

	 	RationalVector2D zero = new RationalVector2D(); 

	 	RationalVector2D first = new RationalVector2D(v1, v2); 
	 	System.out.println(first);

	 	System.out.println(first.add(first));
	 	System.out.println(first.length());

	 	RationalFraction q1 = new RationalFraction(1, 5);
	 	RationalFraction q2 = new RationalFraction(1, 2);

		RationalVector2D secondVector = new RationalVector2D(q1, q2);
	 	System.out.println(first.scalarProduct(secondVector));

	 	System.out.println(first.equals(secondVector));
	 }
}	 