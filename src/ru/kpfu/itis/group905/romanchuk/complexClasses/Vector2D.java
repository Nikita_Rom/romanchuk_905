package ru.kpfu.itis.group905.romanchuk.complexClasses;

public class Vector2D {
		private double x;
		private double y;

		Vector2D() {
		this.x = 0.0;
		this.y = 0.0;
		}

		Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
		}

		protected Vector2D add(Vector2D secondVector) {
		Vector2D result = new Vector2D(
		this.x + secondVector.x,
		this.y + secondVector.y
		);

		return result;
		}

		protected Vector2D sub(Vector2D secondVector) {
		Vector2D result = new Vector2D(
		this.x - secondVector.x,
		this.y - secondVector.y
		);

		return result;
		}

		protected void add2(Vector2D secondVector) {
		this.x += secondVector.x;
		this.y += secondVector.y;
		}

		public double length() {
		return Math.sqrt(
		this.x * this.x + this.y * this.y
		);
		}

		public boolean equals(Vector2D secondVector) {
		if (this.x == secondVector.x && this.y == secondVector.y) {
		return true;
		}

		return false;
		}

		public String toString() {
		return "{" + this.x + " , " + this.y + "}";
		}
}