package ru.kpfu.itis.group905.romanchuk.complexClasses;

public class CheckVector {
	public static void main(String[] args) {
		Vector2D vector = new Vector2D();
		System.out.println(vector);

		Vector2D vector2 = new Vector2D(1, 0);
		System.out.println(vector2);

		Vector2D vector3 = new Vector2D(4, 9);
		System.out.println(vector3);

		Vector2D vector4 = new Vector2D(16, 25);
		System.out.println(vector4);

		Vector2D sum = vector3.add(vector4);
		System.out.println(sum);

		vector.add2(vector2);
		System.out.println(vector);

		Vector2D sub = vector4.sub(vector3);
		System.out.println(sub);

		Vector2D vector5 = new Vector2D(3.0, 4.0);
		double vector5Length = vector5.length();
		System.out.println(vector5Length);

		System.out.println(vector4.equals(vector5));
		System.out.println(vector.equals(vector2));
	}
} 