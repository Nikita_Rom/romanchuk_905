public class RationalFraction {

	private int numerator;
	private int denominator;
	RationalFraction() { this(0,1); }
	// RationalFraction(int numerator, int denominator) {
	//	this.numerator = numerator;
	//	this.denominator = denominator;
	// }	

	RationalFraction(int numerator, int denominator) {
		if (denominator != 0) {
			if (denominator < 0) {
				numerator *= -1;
				denominator *= -1;
			}
			this.numerator = numerator;
			this.denominator = denominator;
		} 
		else {
			System.exit(1);
		}
	}

	// RationalFraction re = new RationalFraction(3,4);
	// RationalFraction re1 = new RationalFraction(3,4);


	public int getNumerator() {
		return this.numerator;
	}
	public int getDenominator() {
		return this.denominator;
	}

	public void reduce() {
		int a = 0;
		if (this.numerator < this.denominator) {
			a = this.numerator;
		} 
		else {
			a = this.denominator;
		}
 		for (int i = a; i > 2; i--) {
			if ((this.numerator % i == 0) & (this.denominator % i == 0)) {
				this.numerator = this.numerator / i;
				this.denominator = this.denominator / i;
			}
		}
	}

	public RationalFraction add(RationalFraction r2) {
		r2.numerator = r2.numerator * this.denominator + this.numerator * r2.denominator;  
  		r2.denominator *= this.denominator;
  	r2.reduce();
    return r2;
    }

    public void add2(RationalFraction r2) {
    r2.numerator = r2.numerator * this.denominator + this.numerator * r2.denominator;  
  	r2.denominator *= this.denominator;
  	r2.reduce(); 
    }

    public RationalFraction sub(RationalFraction r2) {
		r2.numerator = r2.numerator * this.denominator - this.numerator * r2.denominator;  
  		r2.denominator *= this.denominator;
  	r2.reduce();
    return r2;
    }

    public void sub2(RationalFraction r2) {
    	r2.numerator = r2.numerator * this.denominator - this.numerator * r2.denominator;  
  		r2.denominator *= this.denominator;
  	r2.reduce(); 
    }

    public RationalFraction mult(RationalFraction r2) {
        r2.reduce(); 
		RationalFraction result = new RationalFraction(
            this.numerator *= r2.numerator,
            this.denominator *= r2.denominator
            );
        result.reduce();
        return result;
    }

    public void mult2(RationalFraction r2) {
   	 	r2.numerator *= this.denominator;  
  		r2.denominator *= this.denominator;
  	r2.reduce(); 
    }

    public RationalFraction div(RationalFraction r2) {
    	r2.numerator = this.numerator * r2.denominator;
    	r2.denominator = this.denominator * r2.numerator;
    r2.reduce();
    return r2;	
    }

    public void div2(RationalFraction r2) {
    	r2.numerator = this.numerator * r2.denominator;
    	r2.denominator = this.denominator * r2.numerator;
    r2.reduce();
    }


	public String toString() {
    	String string = new String();
    	if ((numerator < 0) & (denominator < 0)) {
    		numerator = -numerator;
    		denominator = -denominator;
    		string = "-" + numerator + "/" + denominator;
    	}
    	else {
	    	if ((numerator < 0) | (denominator < 0)) {
	    		if (numerator < 0) {
	    			numerator = -numerator;
	    		}
	    		if (denominator < 0) {
	    			denominator = -denominator;
	    		}
	    		string = "-" + numerator + "/" + denominator;
	    	}
	    	else {
	    		string = numerator + "/" + denominator;
	    	}

    	}
    return string;	
    }

    public double value() {
        return (double) (this.numerator) / this.denominator;
    }

    public boolean equals(RationalFraction r2) {
    	if ((this.numerator == r2.numerator) & (this.denominator == r2.denominator)) {
    		return true;
    	}
    return false;	
    }

    public int numberPart(int numerator, int denominator) {
    	int c = this.numerator / this.denominator;
    	return c;
    }
    


} 