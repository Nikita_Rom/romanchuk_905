package ru.kpfu.itis.group905.romanchuk.money;

public class Money {
    private int rub = 0;
    private short kopeika = 0;

    Money() {
        this(0, (short) 0);
    }

    public Money(int rub, short kopeika) {
        this.rub = rub;
        if (kopeika < 10) {
            kopeika *= 10;
        }
        this.kopeika = kopeika;
    }

    public Money sub(Money mon) {
        Money vr = new Money(this.rub, this.kopeika);
        short vremennaya = (byte) (vr.kopeika - mon.kopeika);
        if (vremennaya < 0) {
            vr.rub--;
            vr.kopeika = (byte) (100 + vremennaya);
        } else
            vr.kopeika = vremennaya;
        vr.rub = vr.rub - mon.rub;
        return vr;
    }

    public Money mult(double rubkop) {
        double mult = (this.rub * 100 + this.kopeika) / 100;
        mult = Math.round(mult * rubkop * 100.0) / 100.0;
        return krasivo(mult);
    }

    public Money div(Money mon) {
        double x = (this.rub * 100 + this.kopeika) / 100;
        double y = (mon.rub * 100 + mon.kopeika) / 100;
        x = Math.round((x / y) * 100.0) / 100.0;

        return krasivo(x);
    }

    public Money divDrob(double rubkop) {
        return krasivo(Math.round(((this.rub * 100 + this.kopeika) / 100)
                / rubkop * 100.0) / 100.0);
    }

    public Money add(Money mon) {
        return new Money(
                this.rub + mon.rub + (mon.kopeika + this.kopeika) / 100,
                (short) ((mon.kopeika + this.kopeika) % 100));
    }

    public boolean equals(Money money) {
        System.out.println(this.kopeika);
        return this.rub == money.rub && this.kopeika == money.kopeika;
    }

    private Money krasivo(double krasivo) {
        String string = String.valueOf(krasivo);

        return new Money(Integer.parseInt(string.substring(0, string.indexOf("."))),
                Byte.parseByte(string.substring(string.indexOf(".") + 1)));
    }

    public String toString() {
        if (this.kopeika % 10 == 0) {
            return rub + "," + kopeika / 10;
        } else {
            return rub + "," + kopeika;
        }
    }
}
