package ru.kpfu.itis.group905.romanchuk.Kontrol;

import java.util.Scanner;

public class Hamburger implements Cookable {
    Ingredient[] ingredients;
    String whichHam;

    public void prepare() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("What butter do you want today?");
            System.out.println("With hum - 0, jelly-butter sandwich - 1." +
                    " Please, write number.");
            byte num = sc.nextByte();
            if (num == 0) {
                this.whichHam = "jelly-butter sandwich";
                ingredients = new Ingredient[] {
                        Ingredient.Bread, Ingredient.Jelly,
                        Ingredient.Butter, Ingredient.Bread
                };
                break;
            }
            else if (num == 1) {
                this.whichHam = "hum";
                ingredients = new Ingredient[] {
                        Ingredient.Bread, Ingredient.Ham,
                        Ingredient.Souse, Ingredient.Cucumber,
                        Ingredient.Cheese, Ingredient.Bread
                };
                break;
            }
            else {
                System.out.println("You're stupid! Enter what is supposed.");
            }
        }
    }

    @Override
    public Dish cook() {
        return null;
    }
}
// здесь мы узнаем у пользователя, какой бутер он хочет. Даем выбор из двух и
// отправляем в ингридиенты, что нам будет необходимо для создани я такого бутера, который нужен.
// хлеб, ветчина, соус, огурчик, сыр, хлеб