package ru.kpfu.itis.group905.romanchuk.Kontrol;

public interface Cookable {
    void prepare();
    Dish cook();
}
