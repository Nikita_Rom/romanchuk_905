package ru.kpfu.itis.group905.romanchuk.Kontrol;

import ru.kpfu.itis.group905.romanchuk.Kontrol.Cookable;
import ru.kpfu.itis.group905.romanchuk.Kontrol.Dish;
import ru.kpfu.itis.group905.romanchuk.Kontrol.Hamburger;
import ru.kpfu.itis.group905.romanchuk.Kontrol.Salad;

import java.util.Scanner;

public class Main {
    Dish[] orders = new Dish[10];
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("What are you want today?");
        String command = scanner.nextLine();
        start_kitchen(command);
    }

    public static void start_kitchen(String command) {
        switch (command) {
            case "hamburger":
                make_food(new Hamburger());
                break;
            case "salad":
                make_food(new Salad());
                break;
            default:
                System.out.println("Sorry, for now we have only salads and hamburgers. Come later");
                break;
        }
    }


    /*
    Часть 1 (10б)
    Доступный список ингридиентов: Ham, Egg, Jelly, Butter, Bread, Salad, Tomato, Potato, Cheese, Souse, Cucumber
    Подберите нужный тип для сущности Ingredient и объясните (комментарием в соответствующем файле) почему
    */
    public static void make_food (Cookable eat) {
/*
Для салата функция должна предложить ввести последовательно ингредиенты для блюда.
После каждого введенного слова должна появляться информация о том, что ингредиент принят и предложение добавить что-то еще.
Пользовательский ввод заканчивается словом stop.
Учтите, что введенный ингридиет должен быть из списка доступных, причем масло и желе в салате ужасны на вкус,
поэтому надо запретить пользователю их добавлять.
При этом можно запросить до ввода ингридиентов их количество, но в файле они указывать не будут.

Помимо этого хочется иметь возможность скормить программе имя файла (НЕ ПУТЬ, а только имя, сам файл распологается в вашем пакете),
в котором уже через запятую лежит список ингредиентов

Обе возможности оформить отдельными методами внутри класса салата.

Для гамбургера доступны только два вида: jelly-butter sandwich и ham (хлеб, ветчина, соус, огурчик, сыр, хлеб)
Функция должна предложить ввести вид сендвича. На некорректный ввод программа отвечает, что данный вид еды не доступен, зайдите позже,
НО не останавливается, а просит ввести что-то другое

При реализации Cookable ооставьте комментарий о выбранном типе. Почему именно так?
*/
        eat.prepare();

/*
У блюда должен быть внутри массив заполненный ингридиентами в правильном порядке
Для бутера от 0 до n индгридиенты бутера снизу вверх (хлеб два раза, ага)
Для салата точная последовательность того, как вводил ингридиенты пользователь
*/
        Dish dish = eat.cook();

/*
Должно вывестить "Your <имя блюда> with <ингридиенты в точном порядке> is ready"
*/
        System.out.println("Your " + dish + " is ready");
    }

/*
Часть 2 (5б):
Доработать программу так, чтобы в поле orders хранился список всех заказов во время работы кафе.
Доработать start_kitchen так, чтобы запускался бесконечный цикл приема заказов, все заказы сохранять в orders.
При этом у блюда появляется статус (Cooked/Taken) который выставляется рандомно.
На каждой новой иттерации проверить все не отданные заказы и рандомно обновить им статус.

В рандомный момент цикла (не менее 1 иттерации, но менее 10) должен вызываться метод close
В этом методе подсчитать и вывести на экран:
- сколько приготовилось американских сендвичей
- сколько приготовили гамбургеров
- сколько приготовили салатов
- сколько блюд испортилось (испортились все блюда, которые на момент вызова метода в статусе Cooked, т.е. их не забрали)
*/
}