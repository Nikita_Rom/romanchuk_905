package ru.kpfu.itis.group905.romanchuk.Kontrol;

public enum Ingredient {
        Ham, Egg, Jelly, Butter, Bread, Salad, Tomato, Potato, Cheese, Souse,
        Cucumber;
        public static Ingredient [] composition() {
            return new Ingredient[]{
                    Ham, Egg, Jelly, Butter, Bread, Salad, Tomato, Potato, Cheese,
                    Souse, Cucumber
            };
        }
}
// enum - тип, в котором можно перечислять что-то через запятую.
// В данном случае я перечисляю ингридиенты, которые могут существовать в программе.

