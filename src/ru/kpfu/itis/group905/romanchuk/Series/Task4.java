package ru.kpfu.itis.group905.romanchuk.Series; /**
* @author Nikita Romanchuk 
* 11-905
* Task4
*/

import java.util.*;

public class Task4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("write n"); 
		int n = sc.nextInt();
		double sum = 0.0;
		double h1 = 0.0;
		double h2 = 0.0;

		for (int i = 1; i <= n; i++) {
			sum += (2. * i + 1) / (i*i * (i + 1) * (i + 1));
		}
		
		System.out.println(sum);
	}
}