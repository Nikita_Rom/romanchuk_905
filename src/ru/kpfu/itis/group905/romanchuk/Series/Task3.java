package ru.kpfu.itis.group905.romanchuk.Series; /**
* @author Nikita Romanchuk 
* 11-905
* Task3 
*/

import java.util.*;

public class Task3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("write n");
		int n = sc.nextInt();
		double s1 = 1.0;
		double s2 = 1.0;

		for (int i = 1; i <= n; i++) {
			s1 *= 0.8;
		}

		for (int i = 1; i <= n; i++) {
			s2 *= 0.3;
		}
		
		System.out.println(s1 - 3*s2);
	}
}