package ru.kpfu.itis.group905.romanchuk.Series; /**
* @author Nikita Romanchuk 
* 11-905
* Task7
*/

import java.util.*;

public class Task7 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("write n"); 
		int n = sc.nextInt();
		double h1, h2, sum;
		h1 = h2 = 1.0;
		sum = 0.0;

		for (int i = 1; i <= n; i++) {
			h1 *= 2 * i;
			for (int j = 1; j <= i; j++) {
				h2 *= i;
			}
			sum += h1 / h2;
			h2 = 1.0;
		}

		System.out.println(sum);
	}
}	