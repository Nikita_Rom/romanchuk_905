package ru.kpfu.itis.group905.romanchuk.Series; /**
* @author Nikita Romanchuk 
* 11-905
* Task8
*/

import java.util.*;

public class Task8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("write n"); 
		int n = sc.nextInt();
		double h1, h2, sum;
		h1 = h2 = 1.0;
		sum = 0.0;

		for (int i = 1; i <= n; i++) {
			h1 *= i;
			h2 *= 2;
			sum += (h1 * h1) / h2;
		}

		System.out.println(sum);
	}
}	