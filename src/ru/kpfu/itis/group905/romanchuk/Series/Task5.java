package ru.kpfu.itis.group905.romanchuk.Series; /**
* @author Nikita Romanchuk 
* 11-905
* Task5
*/

import java.util.*;

public class Task5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("write n"); 
		int n = sc.nextInt();

		if (n % 2 == 0) {
			System.out.println(0);
		}
		else {
			System.out.println(1);
		}
    }
}