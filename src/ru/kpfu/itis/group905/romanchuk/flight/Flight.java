package ru.kpfu.itis.group905.romanchuk.flight;

public class Flight {
    private int Airline, FlightNo;
    private String SourceAirport, DestAirport;

    Flight() {
        this(0, 0, "", "");
    }

    Flight(int airline, int flightNo, String sourceAirport, String destAirport) {
        this.Airline = airline;
        this.FlightNo = flightNo;
        this.SourceAirport = sourceAirport;
        this.DestAirport = destAirport;
    }

    Flight(String[] airl) {
        this.Airline = Integer.parseInt(String.valueOf(airl[0]));
        this.FlightNo = Integer.parseInt(String.valueOf(airl[1]));
        this.SourceAirport = airl[2];
        this.DestAirport = airl[3];
    }

    public String toString() {
        return "Airline: " + Airline + " FlightNo: " + FlightNo + " SourceAirport: "
                + SourceAirport + " DestAirport: " + DestAirport;
    }

    public int getAirline() {
        return Airline;
    }

    public int getFlightNo() {
        return FlightNo;
    }

    public String getSourceAirport() {
        return SourceAirport;
    }

    public String getDestAirport() {
        return DestAirport;
    }
}
