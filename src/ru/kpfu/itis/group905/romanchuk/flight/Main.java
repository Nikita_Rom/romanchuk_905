package ru.kpfu.itis.group905.romanchuk.flight;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        final int FLIGHTS_COUNT = 1200;
        final String path = "src/ru/kpfu/itis/group905/romanchuk/flight/";
        BufferedReader inputFile
                = new BufferedReader(new FileReader(path + "flights.csv"));
        Flight[] flights = new Flight[FLIGHTS_COUNT];
        String input = inputFile.readLine();
        int i = 0, count = 0;
        while (input != null) {
            String[] flightInfo = input.split(",");
            flights[i] = new Flight(Integer.parseInt(flightInfo[0]),
                    Integer.parseInt(flightInfo[1]), flightInfo[2], flightInfo[3]);

            if (count < Integer.parseInt(flightInfo[0])) {
                count = Integer.parseInt(flightInfo[0]);
            }
            i += 1;
            input = inputFile.readLine();
        }
        inputFile.close();

        int[] s7_airlines = new int[count];
        String[] airport = new String[FLIGHTS_COUNT];

        StringBuilder source = new StringBuilder();

        int count2 = 0;
        for (int j = 0; j < FLIGHTS_COUNT; j++) {
            s7_airlines[flights[j].getAirline() - 1]++;
            if (!source.toString().contains(flights[j].getSourceAirport())) {
                airport[count2] = flights[j].getSourceAirport();
                count2++;
            }
        }
        int max = 0;
        for (int j = 0; j < count; j++) {
            if (s7_airlines[j] > max) {
                max = s7_airlines[j];
            } 
        }
        String name = "";
        for (int j = 0; j < count; j++) {
            if (s7_airlines[j] == max) {
                name = name + (j + 1) + " ";
            }
        }

        System.out.println("Airline with the name flights: " + name + "Count: "
                + max);

        // найдем самый популярный аэропорт. Для этого необходимо пройтись по всем
        // аэропортам и посчитать сколько раз встречается каждый и затем вывести
        // самый популярный. Если несколько с одинаковыми значениями - выводит несколько
        int[] airports = new int[count2];
        for (int j = 0; j < FLIGHTS_COUNT; j++) {
            for (int k = 0; k < count2; k++) {
                if (flights[j].getSourceAirport().equals(airport[k])) {
                    airports[k]++;
                    break;
                }
            }
        }
        int maximum = 0;
        String s = "";
        for (int j = 0; j < count2; j++) {
            if (airports[j] >= maximum) {
                maximum = airports[j];
            }
        }
        for (int j = 0; j < count2; j++) {
            if (airports[j] == maximum) {
                s = s + airport[j] + " ";
            }
        }
        System.out.println("Name of popular airport: " + s
                + "Count:" + maximum);

        int[] num = new int[count];
        for (int j = 0; j < FLIGHTS_COUNT; j++) {
            for (int k = 0; k < FLIGHTS_COUNT; k++) {
                if (flights[j].getAirline() != flights[k].getAirline()) {
                    if (flights[j].getFlightNo() == flights[k].getFlightNo()) {
                        num[flights[k].getAirline() - 1] = 1;
                        num[flights[j].getAirline() - 1] = 1;
                    }
                }
            }
        }
        System.out.print("Airlines number with matching flight numbers: ");
        for (int k = 0; k < count; k++) {
            if (num[k] == 1) {
                System.out.print((k + 1) + " ");
            }
        }
        // выводим номера авикомпаний, добавляя +1, т.к. они были записаны в массиве.
    }
}